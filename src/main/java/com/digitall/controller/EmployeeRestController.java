package com.digitall.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.UriComponentsBuilder;

import com.digitall.model.Employee;
import com.digitall.model.Link;
import com.digitall.repository.EmployeeRepository;

@RestController
public class EmployeeRestController {

	@Autowired
	private EmployeeRepository repository;
	
	@GetMapping(path="/rest/employee/get/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Employee> getEmployeeByID(@PathVariable("id") int id) {
		
		Employee empl = repository.retrieve(id);
		
		HttpStatus httpStatus = HttpStatus.OK;
		if (empl == null) {
			httpStatus = HttpStatus.NOT_FOUND;
		} else {
			UriComponentsBuilder uri = ServletUriComponentsBuilder.fromCurrentRequest();
			uri = uri.replacePath("/rest/employee/get/{id}");
			Link link = new Link();
			link.setHref(uri.build(empl.getManagerId()));
			empl.setManager(link);
		}

		return ResponseEntity.status(httpStatus).body(empl);
	}
	
	@GetMapping(path="/rest/employee/getAll", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Employee> getAllEmployees() {
		return repository.getAll();
	}

	@PostMapping("/rest/employee/create")
	public Employee createEmployee(@RequestBody Employee emp) {
		repository.store(emp);
		return emp;
	}
	
	@GetMapping("/rest/employee/search/{name}")
	public Employee getEmployeeByName(@PathVariable("name") String name) {
		return repository.search(name);
	}
	
	@DeleteMapping("/rest/employee/delete/{id}")
	public Employee deleteEmployeeByID(@PathVariable("id") int id) {
		return repository.delete(id);
	}
}
