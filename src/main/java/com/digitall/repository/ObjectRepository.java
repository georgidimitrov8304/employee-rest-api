package com.digitall.repository;
/**
 * 
 * @author Georgi.Dimitrov
 *
 */
public interface ObjectRepository<T> {
	
	/**
	 * Store <code>t</code> object to repository
	 * @param t object to be stored
	 */
	public void store(T t);

	/**
	 * Retrieve <code>t</code> by id
	 * @param id of object to be retrieved
	 * @return
	 */
	public T retrieve(int id);

	public T search(String name);

	public T delete(int id);
}
