package com.digitall.repository;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.digitall.model.Employee;

@Repository
public class EmployeeRepository implements ObjectRepository<Employee> {

	private Map<Integer, Employee> employeeRepository;

	
	public EmployeeRepository() {
		this.employeeRepository = new HashMap<Integer, Employee>();

		//creating initial data
		Employee manager = new Employee(1, "Mehmed Kadir");
		manager.setManagerId(1);
		this.employeeRepository.put(1, manager);
		
		Employee employee = new Employee(2, "Georgi Dimitrov");
		employee.setManagerId(1);
		this.employeeRepository.put(2, employee);
	}
	
	@Override
	public void store(Employee emp) {
		employeeRepository.put(emp.getId(), emp);		
	}

	@Override
	public Employee retrieve(int id) {
		return employeeRepository.get(id);
	}

	@Override
	public Employee search(String name) {
		Collection<Employee> emps = employeeRepository.values();
		for (Employee emp : emps) {
			if (emp.getName().equalsIgnoreCase(name))
				return emp;
		}
		return null;
	}

	@Override
	public Employee delete(int id) {
		Employee e = employeeRepository.get(id);
		this.employeeRepository.remove(id);
		return e;
	}
	
	public List<Employee> getAll() {
		List<Employee> emps = new ArrayList<Employee>();
		
		for(Employee e : this.employeeRepository.values())
			emps.add(e);
		
		return emps;
	}

}