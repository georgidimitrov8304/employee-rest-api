package com.digitall.model;

import java.net.URI;

public class Link {
	private URI href;

	public URI getHref() {
		return href;
	}

	public void setHref(URI href) {
		this.href = href;
	}
}
