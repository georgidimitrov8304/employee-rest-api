package com.digitall.model;

import java.net.URI;

import javax.xml.bind.annotation.XmlRootElement;



/**
 * Model level employee
 * 
 * @author Georgi.Dimitrov
 *
 */
@XmlRootElement
public class Employee {

	private int id;
	private String name;
	
	private Link manager;
	private int managerId;



	public Employee() {
	}

	public Employee(int i, String n) {
		this.id = i;
		this.name = n;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
		
	public Link getManager() {
		return manager;
	}

	public void setManager(Link manager) {
		this.manager = manager;
	}
	
	public int getManagerId() {
		return managerId;
	}

	public void setManagerId(int managerId) {
		this.managerId = managerId;
	}
	
	@Override
	public String toString() {
		return id + "," + name;
	}
}